import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DomProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DomainProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DomainProvider Provider');
  }		

  check_sub_domain(sub){

  	return this.http.get<any>('//geekinn.api.resourceinn.com/api/v1/tenants/check_subdomain?subdomain=' + sub);
  }

}
