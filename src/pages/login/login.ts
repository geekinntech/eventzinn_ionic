import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { DomainProvider } from '../../providers/domain/domain';






@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {

	constructor(public navCtrl: NavController,
		public loadingCtrl: LoadingController,
		public domain_provider: DomainProvider,
		private alertCtrl: AlertController) {

		this.presentPrompt();
	}

	presentPrompt() {
		let alert = this.alertCtrl.create({
			title: 'Enter your Domain',
			enableBackdropDismiss: false,
			inputs: [
				{
					name: 'domain',

				},

			],
			buttons: [
				{
					text: 'Done',
					role: '',
					handler: data => {

						if(data.domain){

							var load = this.presentLoadingDefault();
							load.present();
						console.log('Done clicked');
						this.domain_provider.check_sub_domain(data.domain).subscribe(data => {



							console.log('great');
							console.log(data.status);
							console.log(data.data);
							console.log(data.headers);
							alert.dismiss();
							load.dismiss();			

						}, error => {

							this.presentAlert();
							load.dismiss();

						})
					}
					else {
						this.showError();
						
					}
					return false;


					}
				},

			],

		});

		alert.present();


	}
	presentAlert() {
		let alert = this.alertCtrl.create({
			title: 'Error',
			subTitle: 'Not Found',
			buttons: ['Dismiss']
		});
		alert.present();
	}

	presentLoadingDefault() {
  let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  return loading;
  
}
showError() {
		let alert = this.alertCtrl.create({
			title: 'Error',
			subTitle: 'Field cannot be empty',
			buttons: ['Dismiss']
		});
		alert.present();
	}


}
